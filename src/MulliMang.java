import javafx.animation.*;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import javafx.util.Duration;
import java.util.Random;


public class MulliMang extends Application {

    static Group root;
    Scene scene;
    Button button;
    Button restartButton;
    private int skoor;
    private boolean gameOver;
    private boolean resetScreen;
    private Label skooriLabel;
    private Random rand;
    private Stage s;

    public static void main(String args[]) {
        Application.launch(args);

    }

    @Override
    public void start(Stage s) {
        rand = new Random();
        root = new Group();
        scene = new Scene(root, 800, 800);
        button = new Button();
        restartButton = new Button();
        button.setText("Alusta mängu!");
        gameOver = false;
        resetScreen = false;
        skoor = 0;

        /*Nupp ja selle asukoht*/
        root.getChildren().add(button);
        this.s = s;
        this.s.setScene(scene);
        s.show();
        button.setLayoutX(350);
        button.setLayoutY(400);

        skooriLabel = new Label("Skoor:0");

        root.getChildren().add(skooriLabel);
        /*Nupu vajutamisel genereeritakse ringid ja mäng algab*/
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                root.getChildren().remove(button);
                ringiGeneraator();
            }
        });
        /*Restardinupp*/
        restartButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                gameOver  = false;
                skoor = 0;
                resetScreen = false;
                start(s);
                root.getChildren().remove(button);
            }
        });
    }
    /*Ringide sagedus*/
    public void ringiGeneraator() {
        Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(1), ev -> {
            if(resetScreen == true){
                return;
            }else
            if (gameOver == false) {
                teeRing();
            } else {
                try {
                    /*Peatatakse mäng ning visatakse kollane ekraan, pakutakse uuesti mängimise võimalust*/
                    this.stop();
                    root = new Group();
                    scene = new Scene(root, 800, 800, Color.YELLOW);
                    resetScreen = true;
                    s.setScene(scene);
                    restartButton.setText("Mängi uuesti!");
                    restartButton.setLayoutX(350);
                    restartButton.setLayoutY(400);
                    root.getChildren().add(restartButton);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }));
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();

    }

    /*Kood on võetud http://stackoverflow.com/questions/363681/generating-random-integers-in-a-specific-range*/
    public int randInt(int min, int max) {

        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }



    /*Ringi andmed*/
    void teeRing() {
        int raadius = randInt(20, 100);
        System.out.println(raadius);

        /*Värvi random generator on võetud http://stackoverflow.com/questions/4246351/creating-random-colour-in-java*/
        double r = rand.nextFloat();
        double g = rand.nextFloat();
        double b = rand.nextFloat();
        Color randomVarv = new Color(r, g, b, 1);
        Circle ring = new Circle(randInt(0 + raadius, 800 - raadius), 0 - 110, raadius, randomVarv);

        /*Palli kukkumise kiirus ja pallipiirid*/
        TranslateTransition muutused = new TranslateTransition(Duration.millis(randInt(1600, 4000)));
        muutused.setToX(1);
        muutused.setToY(1050);

        ParallelTransition animatsioon = new ParallelTransition(ring, muutused);
        animatsioon.setCycleCount(1);
        animatsioon.play();
        animatsioon.setOnFinished(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                gameOver = true;

            }
        });
        root.getChildren().add(ring);
    /*Hiirevajutusega ringil kaob ring ning lisatakse skoorile punkt ja kuvatakse see*/
        ring.setOnMousePressed(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent mouseEvent) {
                root.getChildren().remove(ring);
                skoor++;
                animatsioon.stop();
                skooriLabel.setText("Skoor:" + skoor);
            }
        });

    }

}